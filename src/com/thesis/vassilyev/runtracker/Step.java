package com.thesis.vassilyev.runtracker;

import java.util.UUID;

import org.json.JSONException;
import org.json.JSONObject;


public class Step {

	private static final String JSON_ID = "id";
	private static final String JSON_TIMESTAMP = "timestamp";
	private static final String JSON_X = "x";
	private static final String JSON_Y = "y";
	private static final String JSON_Z = "z";

	private UUID mId;
    private long mTimeStamp;
    private double mX;
    private double mY;
    private double mZ;
    private double mHeading;

    public Step() {
        mId = UUID.randomUUID();
        //mDate = new Date();
    }
    
    public Step(long timeStamp, double x, double y, double z, double heading){
    	mId = UUID.randomUUID();
    	mTimeStamp = timeStamp;
    	mX = x;
    	mY = y;
    	mZ = z;
    	mHeading = heading;
    }
    
    public Step(JSONObject json) throws JSONException{
    	mId = UUID.fromString(json.getString(JSON_ID));
    	mTimeStamp = json.getLong(JSON_TIMESTAMP);
    	mX = json.getDouble(JSON_X);
    	mY = json.getDouble(JSON_Y);
    	mZ = json.getDouble(JSON_Z);
    }

    public JSONObject toJSON () throws JSONException{
    	JSONObject json  = new JSONObject();
    	json.put(JSON_ID, mId.toString());
    	json.put(JSON_TIMESTAMP, mTimeStamp);
    	json.put(JSON_X, mX);
    	json.put(JSON_Y, mY);
    	json.put(JSON_Z, mZ);

    	return json;
    }
    
    @Override
    public String toString() {
        return String.valueOf(mTimeStamp);
    }

	public double getTimeStamp() {
		return mTimeStamp;
	}

	public void setTimeStamp(long timeStamp) {
		mTimeStamp = timeStamp;
	}

	public double getX() {
		return mX;
	}

	public void setX(double x) {
		mX = x;
	}

	public double getY() {
		return mY;
	}

	public void setY(double y) {
		mY = y;
	}

	public double getZ() {
		return mZ;
	}

	public void setZ(double z) {
		mZ = z;
	}
	
	public double getHeading() {
		return mHeading;
	}

	public void setHeading(double heading) {
		mHeading = heading;
	}

}