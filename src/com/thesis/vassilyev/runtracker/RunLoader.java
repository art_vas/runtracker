package com.thesis.vassilyev.runtracker;

import android.content.Context;

public class RunLoader extends DataLoader<Run>{
	
	private long mRunId;
	
	public RunLoader(Context context, long runId) {
		super(context);
		mRunId = runId;
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public Run loadInBackground(){
		return RunManager.get(getContext()).getRun(mRunId);
	}
}
