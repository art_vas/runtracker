package com.thesis.vassilyev.runtracker;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayDeque;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Bundle;
import android.renderscript.Sampler.Value;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.text.style.UpdateLayout;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class RunFragment extends Fragment{

	private static final String ARG_RUN_ID = "RUN_ID";
	private static final int LOAD_RUN = 0;
	private static final int LOAD_LOCATION = 1;

	
	//Estimate location using GPS
	private BroadcastReceiver mLocationReceiver = new LocationReceiver(){
		
		@Override
		protected void onLocationReceived(Context context, Location loc){
			
			if (!mRunManager.isTrackingRun())
				return;

			
			mLastLocation = loc;
			
			if(isVisible()){
				updateUI();
			}
		}
		
		@Override
		protected void onProviderEnabledChanged(boolean enabled){
			int toastText = enabled ? R.string.gps_enabled : R.string.gps_disabled;
			Toast.makeText(getActivity(), toastText, Toast.LENGTH_LONG).show();
		}
		
	};
	
	//Sensor event listener
	private final SensorEventListener mListener = new SensorEventListener() {
		@Override
	    public void onSensorChanged(SensorEvent event) {
	        
			if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER){
				
				mSensorX = event.values[0];
				mSensorY = event.values[1];
			    mSensorZ = event.values[2];
			    mSensorTimeStamp = event.timestamp;
			    mLabelX.setText(Double.toString(mSensorX));
				mLabelY.setText(Double.toString(mSensorY));
				mLabelZ.setText(Double.toString(mSensorZ));

			}
			if (event.sensor.getType() == Sensor.TYPE_GYROSCOPE){
				mGyroY = event.values[1];
			}
			if (event.sensor.getType() == Sensor.TYPE_ORIENTATION){
				
				filter.add(Math.toRadians(event.values[0]));
				mHeading = Math.toDegrees(filter.average());
				if (mHeading < 0)
					mHeading += 360;
				mCompassLabel.setText(Double.toString(mHeading));
			}
			if(event.sensor.getType() == Sensor.TYPE_STEP_DETECTOR){
				Step st = new Step(mStamp, mSensorX, mSensorY, mSensorZ, mHeading);
				mRunManager.insertLocation(st);
				//Shows the difference in time between two steps in nanoseconds
				mTimeStamp.setText(String.valueOf(event.timestamp)+ "\n Diff: " + (String.valueOf(event.timestamp-  mStamp))
						+ "\n Detected: " + (String.valueOf(event.values[0])
								+ "\n NoSteps:" + String.valueOf(mStepCounter)));
				
				mStamp = event.timestamp;
				//Count the amount of steps from application launch
				mStepCounter++;
				
				//write the date into file after each step
				writer = null;
				file = new File(getActivity().getExternalFilesDir(null).getAbsolutePath(), "steps.db");
				try{
					
					OutputStream out = new FileOutputStream(file, true);
					writer = new OutputStreamWriter(out);
					writer.write(String.valueOf(event.timestamp) +
							"," + mLabelX.getText().toString() + 
							"," + mLabelY.getText().toString() + 
							"," + mLabelZ.getText().toString()+ 
							"," + mGyroY + 
							"," + mCompassLabel.getText().toString() + "\n");
				}catch (IOException e) {
			        // Unable to create file, likely because external storage is
			        // not currently mounted.
			        Log.w("ExternalStorage", "Error writing " + file, e);
			    }finally{
					if(writer != null){
						try {
							writer.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				
			}
			if(event.sensor.getType() == Sensor.TYPE_STEP_COUNTER){
				if (mActivityRunning) {
		            mCount.setText(String.valueOf(event.values[0]));
		        }
			}
	    }


        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };
	
    Writer writer;
    File file;
    
	private RunManager mRunManager;
	private SensorManager mSensorManager;
	private Sensor mAccelerometer;
	private Sensor mStepDetector;
	private Sensor mCompass;
	private Sensor mGyro;
	
	boolean mActivityRunning;
	
	AngleLowpassFilter filter = new AngleLowpassFilter();
	
	private double mSensorX;
	private double mSensorY;
	private double mSensorZ;
	private double mGyroY;
	private double mHeading;
	private long mSensorTimeStamp;
	private int mStepCounter;
	private long mStamp;
	private static long mId;
	
	private Run mRun;
	private Location mLastLocation;
	private Button mStartButton, mStopButton, mMapButton;
	private TextView mStartedTextView, mLatitudeTextView,
		mLongitudeTextView, mAltitudeTextView, mDurationTextView, mCount,
		mLabelX, mLabelY, mLabelZ, mTimeStamp, mCompassLabel;
	
	public static RunFragment newInstance(long runId) {
		Bundle args = new Bundle();
		mId = runId;
		args.putLong(ARG_RUN_ID, runId);
		RunFragment rf = new RunFragment();
		rf.setArguments(args);
		return rf;
	}

	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		mRunManager = RunManager.get(getActivity());
		
		// Проверить идентификатор Run и получить объект серии
		Bundle args = getArguments();
		if (args != null) {
			long runId = args.getLong(ARG_RUN_ID, -1);
			if (runId != -1) {
				LoaderManager lm = getLoaderManager();
				lm.initLoader(LOAD_RUN, args, new LocationLoaderCallbacks());
				mLastLocation = mRunManager.getLastLocationForRun(runId);

			}
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
		Bundle savedInstanceState) {
		
		mSensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
		
		View view = inflater.inflate(R.layout.fragment_run, container, false);
		mStartedTextView = (TextView)view.findViewById(R.id.run_startedTextView);
		mLatitudeTextView = (TextView)view.findViewById(R.id.run_latitudeTextView);
		mLongitudeTextView = (TextView)view.findViewById(R.id.run_longitudeTextView);
		mAltitudeTextView = (TextView)view.findViewById(R.id.run_altitudeTextView);
		mDurationTextView = (TextView)view.findViewById(R.id.run_durationTextView);
		
		mLabelX = (TextView)view.findViewById(R.id.labelX);
		mLabelY = (TextView)view.findViewById(R.id.labelY);
		mLabelZ = (TextView)view.findViewById(R.id.labelZ);
		mTimeStamp = (TextView)view.findViewById(R.id.labelTimeS);
		
		mCompassLabel = (TextView)view.findViewById(R.id.label_compass);
		
		mCount = (TextView)view.findViewById(R.id.count);
		mStartButton = (Button)view.findViewById(R.id.run_startButton);
		mStartButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (mRun == null) {
					mRun = mRunManager.startNewRun();
				} else {
					mRunManager.startTrackingRun(mRun);
				}
				mActivityRunning = true;
		        Sensor countSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
		        if (countSensor != null) {
		            mSensorManager.registerListener(mListener, countSensor, SensorManager.SENSOR_DELAY_UI);
		        } else {
		            Toast.makeText(getActivity(), "Count sensor not available!", Toast.LENGTH_LONG).show();
		        }
				mSensorManager.registerListener(mListener, mAccelerometer, 50000);
				mSensorManager.registerListener(mListener, mStepDetector, SensorManager.SENSOR_DELAY_FASTEST);
				mSensorManager.registerListener(mListener, mCompass, SensorManager.SENSOR_DELAY_FASTEST);
				mSensorManager.registerListener(mListener, mGyro, SensorManager.SENSOR_DELAY_FASTEST);
				mStepCounter = 0;
				
				updateUI();
				
			}
		});
		mStopButton = (Button)view.findViewById(R.id.run_stopButton);
		mStopButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mRunManager.stopRun();
				mSensorManager.unregisterListener(mListener);
				mActivityRunning = false;
				mStepCounter = 0;
				updateUI();
			}
		});
		
		mMapButton = (Button)view.findViewById(R.id.run_mapButton);
		mMapButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getActivity(), RunMapActivity.class);
				//i.putExtra(RunMapActivity.EXTRA_RUN_ID, mRun.getId());
				i.putExtra(RunMapActivity.EXTRA_RUN_ID, mId);
				startActivity(i);
			}
		});
		
		updateUI();
		
	return view;
	}
	
	@Override
	public void onStart(){
		super.onStart();
		mStepCounter = 0;
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mStepDetector = mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
        mCompass = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
        mGyro = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
		getActivity().registerReceiver(mLocationReceiver, new IntentFilter(RunManager.ACTION_LOCATION));
	}

	@Override
	public void onStop(){
		
		getActivity().unregisterReceiver(mLocationReceiver);
		mSensorManager.unregisterListener(mListener);
		mActivityRunning = false;
		mStepCounter = 0;
		super.onStop();
	}
	
	private void updateUI(){
		boolean trackingThisRun = mRunManager.isTrackingRun();

		if(mRun != null){
			mStartedTextView.setText(mRun.getStartDate().toString());
		}
		
		int durationSeconds = 0;
		
		/*mLabelX.setText(Double.toString(mSensorX));
		mLabelY.setText(Double.toString(mSensorY));
		mLabelZ.setText(Double.toString(mSensorZ));*/
		//mTimeStamp.setText(long.class.toString(mSensorTimeStamp));
		
		
		
		if(mRun != null && mLastLocation != null){
			durationSeconds = mRun.getDurationSeconds(mLastLocation.getTime());
			mLatitudeTextView.setText(Double.toString(mLastLocation.getLatitude()));
			mLongitudeTextView.setText(Double.toString(mLastLocation.getLongitude()));
			mAltitudeTextView.setText(Double.toString(mLastLocation.getAltitude()));
			mMapButton.setEnabled(true);
		//}else{
			//mMapButton.setEnabled(false);
		}
		
		mDurationTextView.setText(Run.formatDuration(durationSeconds));
		
		boolean started = mRunManager.isTrackingRun();
		
		mStartButton.setEnabled(!started);
		mStopButton.setEnabled(started && trackingThisRun);

	}

	private class RunLoaderCallbacks implements LoaderCallbacks<Run> {
		
		@Override
		public Loader<Run> onCreateLoader(int id, Bundle args) {
			return new RunLoader(getActivity(), args.getLong(ARG_RUN_ID));
		}
		
		@Override
		public void onLoadFinished(Loader<Run> loader, Run run) {
			mRun = run;
			updateUI();
		}
		
		@Override
		public void onLoaderReset(Loader<Run> loader) {
			// Ничего не делать
		}
	}
	
	private class LocationLoaderCallbacks implements LoaderCallbacks<Location> {
		@Override
		public Loader<Location> onCreateLoader(int id, Bundle args) {
			return new LastLocationLoader(getActivity(), args.getLong(ARG_RUN_ID));
		}
		
		@Override
		public void onLoadFinished(Loader<Location> loader, Location location) {
			mLastLocation = location;
			updateUI();
		}
			
		@Override
		public void onLoaderReset(Loader<Location> loader) {
			// Ничего не делать
		}
	}
	
	public class AngleLowpassFilter {

	    private final int LENGTH = 10;

	    private double sumSin, sumCos;

	    private ArrayDeque<Double> queue = new ArrayDeque<Double>();

	    public void add(double radians){

	        sumSin += Math.sin(radians);

	        sumCos += Math.cos(radians);

	        queue.add(radians);

	        if(queue.size() > LENGTH){

	            double old = queue.poll();

	            sumSin -= Math.sin(old);

	            sumCos -= Math.cos(old);
	        }
	    }

	    public double average(){

	        int size = queue.size();

	        return (double) Math.atan2(sumSin / size, sumCos / size);
	    }
	}

}
