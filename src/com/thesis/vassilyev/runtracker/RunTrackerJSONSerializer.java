package com.thesis.vassilyev.runtracker;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import android.util.Log;


	public class RunTrackerJSONSerializer {
		private Context mContext;
		private String mFilename;
		
		public RunTrackerJSONSerializer(Context c, String s){
			mContext = c;
			mFilename = s;
		}
		
		public void saveSteps(ArrayList<Step> steps) throws JSONException, IOException{
			JSONArray array = new JSONArray();
			for(Step c : steps){
				array.put(c.toJSON());
			}
			Writer writer = null;
			
			Boolean ifSDPresend = android.os.Environment.getExternalStorageState().equals(
					android.os.Environment.MEDIA_MOUNTED);
			if(ifSDPresend){
				File file = new File(mContext.getExternalFilesDir(null).getAbsolutePath(), "settings.cfg");
				try{
					
					OutputStream out = new FileOutputStream(file);
					writer = new OutputStreamWriter(out);
					writer.write(array.toString());
				}catch (IOException e) {
			        // Unable to create file, likely because external storage is
			        // not currently mounted.
			        Log.w("ExternalStorage", "Error writing " + file, e);
			    }finally{
					if(writer != null){
						writer.close();
					}
				}
			}
			else{
				try{
					
					OutputStream out = mContext
							.openFileOutput(mFilename, Context.MODE_PRIVATE);
					writer = new OutputStreamWriter(out);
					writer.write(array.toString());
				}finally{
					if(writer != null){
						writer.close();
					}
				}
			}
		}
	}
