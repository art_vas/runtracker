package com.thesis.vassilyev.runtracker;

import java.util.Date;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorWrapper;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Location;
import android.util.Log;
import android.widget.Toast;

public class RunDatabaseHelper extends SQLiteOpenHelper {

	private static final String DB_NAME = "runs.sqlite"; 
	private static final int VERSION = 1;
	
	private static final String TABLE_RUN = "run";
	private static final String COLUMN_RUN_ID = "_id";
	private static final String COLUMN_RUN_START_DATE = "start_date";
	
	private static final String TABLE_LOCATION = "location";
	private static final String COLUMN_LOCATION_XAXIS = "xaxis";
	private static final String COLUMN_LOCATION_YAXIS = "yaxis";
	private static final String COLUMN_LOCATION_ZAXIS = "zaxis";
	private static final String COLUMN_LOCATION_TIMESTAMP = "timestamp";
	private static final String COLUMN_LOCATION_HEADING = "heading";
	private static final String COLUMN_LOCATION_RUN_ID = "run_id";

	public long insertLocation(long runId, Step st) {
		ContentValues cv = new ContentValues();
		cv.put(COLUMN_LOCATION_XAXIS, st.getX());
		cv.put(COLUMN_LOCATION_YAXIS, st.getY());
		cv.put(COLUMN_LOCATION_ZAXIS, st.getZ());
		cv.put(COLUMN_LOCATION_TIMESTAMP, st.getTimeStamp());
		cv.put(COLUMN_LOCATION_HEADING, st.getHeading());
		cv.put(COLUMN_LOCATION_RUN_ID, runId);
		return getWritableDatabase().insert(TABLE_LOCATION, null, cv);
		}

	
	public RunDatabaseHelper(Context context){
		super(context, DB_NAME, null, VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("create table run ( " +
		"_id integer primary key autoincrement, start_date integer)");
		db.execSQL("create table location ("
				+ " timestamp integer, xaxis double, yaxis double, zaxis double, heading double,"
				+ " provider varchar(100), run_id integer references run(_id))");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}
	
	public long insertRun(Run run){
		ContentValues cv = new ContentValues();
		cv.put(COLUMN_RUN_START_DATE, run.getStartDate().getTime());
		return getWritableDatabase().insert(TABLE_RUN, null, cv);
	}
	
	public RunCursor QueryRuns(){
		Cursor wrapped = getReadableDatabase().query(TABLE_RUN, null,null,null,null,null, COLUMN_RUN_START_DATE +" asc");
		return new RunCursor(wrapped);
	}
	
	public RunCursor queryRun(long id) {
		Cursor wrapped = getReadableDatabase().query(TABLE_RUN,
		null, // Все столбцы
		COLUMN_RUN_ID + " = ?", // Поиск по идентификатору серии
		new String[]{ String.valueOf(id) }, // С этим значением
		null, // group by
		null, // order by
		null, // having
		"1"); // 1 строка
		return new RunCursor(wrapped);
	}

	public LocationCursor queryLastLocationForRun(long runId) {
		Cursor wrapped = getReadableDatabase().query(TABLE_LOCATION,
		null, // Все столбцы
		COLUMN_LOCATION_RUN_ID + " = ?", // Ограничить заданной серией
		new String[]{ String.valueOf(runId) },
		null, // group by
		null, // having
		COLUMN_LOCATION_TIMESTAMP + " desc", // Сначала самые новые
		"1"); // limit 1
		return new LocationCursor(wrapped);
	}
	
	public static class RunCursor extends CursorWrapper{
		
		public RunCursor(Cursor c){
			super(c);
		}
		
		public Run getRun(){
			if(isBeforeFirst() || isAfterLast()){
				return null;
			}
			
			Run run = new Run();
			long runId = getLong(getColumnIndex(COLUMN_RUN_ID));
			run.setId(runId);
			long startDate = getLong(getColumnIndex(COLUMN_RUN_START_DATE));
			run.setStartDate(new Date(startDate));
			return run;
		}
	}
	
	public static class LocationCursor extends CursorWrapper {
		public LocationCursor(Cursor c) {
			super(c);
		}
		
		public Location getLocation() {
			if (isBeforeFirst() || isAfterLast())
				return null;
			// Сначала получаем поставщика для использования конструктора
			String provider = getString(getColumnIndex(COLUMN_LOCATION_HEADING));
			Location loc = new Location(provider);
			// Заполнение остальных свойств
			loc.setLongitude(getDouble(getColumnIndex(COLUMN_LOCATION_XAXIS)));
			loc.setLatitude(getDouble(getColumnIndex(COLUMN_LOCATION_YAXIS)));
			loc.setAltitude(getDouble(getColumnIndex(COLUMN_LOCATION_ZAXIS)));
			loc.setTime(getLong(getColumnIndex(COLUMN_LOCATION_TIMESTAMP)));
			loc.setBearing(getFloat(getColumnIndex(COLUMN_LOCATION_HEADING)));
			
			return loc;
		}
	}

	public LocationCursor queryLocationsForRun(long runId) {
		Cursor wrapped = getReadableDatabase().query(TABLE_LOCATION,
				null,
				COLUMN_LOCATION_RUN_ID + " = ?", // Отбор по заданной серии
				new String[]{ String.valueOf(runId) },
				null, // group by
				null, // having
				COLUMN_LOCATION_TIMESTAMP + " asc"); // Упорядочить
		return new LocationCursor(wrapped);
		 // по временной метке
		}


}
