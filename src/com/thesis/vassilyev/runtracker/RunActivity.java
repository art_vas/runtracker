package com.thesis.vassilyev.runtracker;

import android.support.v4.app.Fragment;
import android.widget.Toast;

public class RunActivity extends SingleFragmentActivity{
	
	public static final String EXTRA_RUN_ID =
			"com.thesis.vassilyev.runtracker.run_id";
	
	@Override
	protected Fragment createFragment(){
		long runId = getIntent().getLongExtra(EXTRA_RUN_ID, -1);
		//Toast.makeText(this, Long.toString(runId), Toast.LENGTH_SHORT).show();
		if(runId != -1){
			return RunFragment.newInstance(runId);
		}else{
			return new RunFragment();
		}
	}
	
}
