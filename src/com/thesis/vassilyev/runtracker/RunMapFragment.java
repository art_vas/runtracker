package com.thesis.vassilyev.runtracker;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager.LayoutParams;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.thesis.vassilyev.runtracker.RunDatabaseHelper.LocationCursor;

public class RunMapFragment extends SupportMapFragment implements LoaderCallbacks<Cursor>{
	private static final String ARG_RUN_ID = "RUN_ID";
	private static final int LOAD_LOCATIONS = 0;
	private static final int REQUEST_STEP_LENGTH = 0;
	private static final String DIALOG_STEP_LENGTH = "step_length";
	View v;
	
	private GoogleMap mGoogleMap;
	private double mStepLength;
	private LocationCursor mLocationCursor;
	private MarkerOptions startMarkerOptions;
	
	public static RunMapFragment newInstance(long runId) {
		Bundle args = new Bundle();
		args.putLong(ARG_RUN_ID, runId);
		RunMapFragment rf = new RunMapFragment();
		rf.setArguments(args);
		return rf;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);			
		
		Bundle args = getArguments();
		
		if (args != null) {
			
			long runId = args.getLong(ARG_RUN_ID, -1);
			if (runId != -1) {
				LoaderManager lm = getLoaderManager();
				lm.initLoader(LOAD_LOCATIONS, args, this);
			}
		}
		
		//dialog window for choosing a step length
		final AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
		final EditText input = new EditText(getActivity());
		input.setInputType(InputType.TYPE_CLASS_NUMBER);
		alert.setView(input);
		alert.setTitle("Step Length (cm)");
		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
		    public void onClick(DialogInterface dialog, int whichButton) {
		      mStepLength = Double.parseDouble(input.getText().toString().trim())*0.00001;
		    }
		});

		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		    public void onClick(DialogInterface dialog, int whichButton) {
			dialog.cancel();
		    }
		});
		alert.show(); 
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent,
		Bundle savedInstanceState) {
		v = super.onCreateView(inflater, parent, savedInstanceState);
		//inflater.inflate(R.layout.fragment_map, parent);
		// stash a reference to GoogleMap
		mGoogleMap = getMap();
		
		//TUT coordinates
		LatLng TUT = new LatLng(61.449776, 23.854630);
		
		//move camera to TUT
		mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(TUT, 19));
		
		mGoogleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
		
		//draw the route by click
		mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
			
			@Override
			public void onMapClick(LatLng point) {
				
				if(startMarkerOptions == null){
					startMarkerOptions = new MarkerOptions()
					.position(point)
					.title(getString(R.string.run_start));
					mGoogleMap.addMarker(startMarkerOptions);
					/*mGoogleMap.addMarker(new MarkerOptions()
						.position(FindPointAtDistanceFrom(point, DegreesToRadians(120), mStepLength))
						.title("2nd"));*/
					updateUI(point);
				}
				else{
					startMarkerOptions.position(point);
					mGoogleMap.clear();
					mGoogleMap.addMarker(startMarkerOptions);
					updateUI(point);
				}

			}
		});
		
		//mGoogleMap.setMyLocationEnabled(true);
		return v;
	}
	
	//9, the step length and the azimuth
	public static LatLng FindPointAtDistanceFrom(LatLng startPoint, double initialBearingRadians, double distanceKilometres){
	    double radiusEarthKilometres = 6371.01;
	    double distRatio = distanceKilometres / radiusEarthKilometres;
	    double distRatioSine = Math.sin(distRatio);
	    double distRatioCosine = Math.cos(distRatio);

	    double startLatRad = DegreesToRadians(startPoint.latitude);
	    double startLonRad = DegreesToRadians(startPoint.longitude);

	    double startLatCos = Math.cos(startLatRad);
	    double startLatSin = Math.sin(startLatRad);

	    double endLatRads = Math.asin((startLatSin * distRatioCosine) + (startLatCos * distRatioSine * Math.cos(initialBearingRadians)));

	    double endLonRads = startLonRad + Math.atan2( Math.sin(initialBearingRadians) * distRatioSine * startLatCos,
	            distRatioCosine - startLatSin * Math.sin(endLatRads));

	    return new LatLng(RadiansToDegrees(endLatRads), RadiansToDegrees(endLonRads));
	}

	public static double DegreesToRadians(double degrees)
	{
	    double degToRadFactor = Math.PI / 180;
	    return degrees * degToRadFactor;
	}

	public static double RadiansToDegrees(double radians)
	{
	    double radToDegFactor = 180 / Math.PI;
	    return radians * radToDegFactor;
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		long runId = args.getLong(ARG_RUN_ID, -1);
		Toast.makeText(getActivity(), Long.toString(runId), Toast.LENGTH_SHORT).show();
		return new LocationListCursorLoader(getActivity(), runId);
	}
	
	private void updateUI(LatLng start) {
		if (mGoogleMap == null || mLocationCursor == null){
			
			return;
		}
		// set up an overlay on the map for this run's locations
        // create a polyline with all of the points
		
		PolylineOptions line = new PolylineOptions();
		// also create a LatLngBounds so we can zoom to fit
		LatLngBounds.Builder latLngBuilder = new LatLngBounds.Builder();
		line.add(start);
		LatLng latLng = start;
		// iterate over the locations
		mLocationCursor.moveToFirst();
		while (!mLocationCursor.isAfterLast()) {
			Location loc = mLocationCursor.getLocation();
			latLng = FindPointAtDistanceFrom(latLng, DegreesToRadians(loc.getBearing()), mStepLength);
			line.add(latLng);
			latLngBuilder.include(latLng);
			mLocationCursor.moveToNext();
		}
		// add route on map
		mGoogleMap.addPolyline(line);
		
		/*Display display = getActivity().getWindowManager().getDefaultDisplay();
	
		LatLngBounds latLngBounds = latLngBuilder.build();
		CameraUpdate movement = CameraUpdateFactory.newLatLngBounds(latLngBounds,
		display.getWidth(), display.getHeight(), 15);
		mGoogleMap.moveCamera(movement);*/
	}
	
	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		mLocationCursor = (LocationCursor)cursor;
	}
	
	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		// stop using data
		mLocationCursor.close();
		mLocationCursor = null;
	}

	

	
	
}
