package com.thesis.vassilyev.runtracker;

import com.thesis.vassilyev.runtracker.RunDatabaseHelper.LocationCursor;
import com.thesis.vassilyev.runtracker.RunDatabaseHelper.RunCursor;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;

public class RunManager {

	private final static String TAG = "RunManager";
	
	private final static String PREFS_FILE = "runs";
	private final static String PREF_CURRENT_RUN_ID = "RunManager.currentRunId";
	
	public static final String ACTION_LOCATION = 
			"com.thesis.vassilyev.runtracker.ACTION_LOCATION";
	
	private static RunManager sRunManager;
	private Context mAppContext;
	private LocationManager mLocationManager;
	private RunDatabaseHelper mHelper;
	private SharedPreferences mPrefs;
	private long mCurrentId;
	
	private RunManager(Context appContext){
		mAppContext = appContext;
		mLocationManager = (LocationManager)mAppContext.getSystemService(Context.LOCATION_SERVICE);
		mHelper = new RunDatabaseHelper(mAppContext);
		mPrefs = mAppContext.getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE);
		mCurrentId = mPrefs.getLong(PREF_CURRENT_RUN_ID, -1);
	}
	
	public static RunManager get(Context c){
		if(sRunManager == null){
			sRunManager = new RunManager(c.getApplicationContext());
		}
		return sRunManager;
	}
	
	private PendingIntent getLocationPendingIntent(boolean shouldCreate){
		Intent broadcast = new Intent(ACTION_LOCATION);
		int flags = shouldCreate ? 0 : PendingIntent.FLAG_NO_CREATE;
		return PendingIntent.getBroadcast(mAppContext, 0, broadcast, flags);
	}
	
	public void startLocationUpdates(){
		String provider = LocationManager.GPS_PROVIDER;
		PendingIntent pi = getLocationPendingIntent(true);
		mLocationManager.requestLocationUpdates(provider, 0, 0, pi);		
	}
	
	public void stopLocationUpdates(){
		PendingIntent pi = getLocationPendingIntent(false);
		if(pi != null){
			mLocationManager.removeUpdates(pi);
			pi.cancel();
		}
	}
	
	public boolean isTrackingRun(){
		return getLocationPendingIntent(false) != null;
	}
	
	public Run startNewRun() {
		// Вставка объекта Run в базу данных
		Run run = insertRun();
		// Запуск отслеживания серии
		startTrackingRun(run);
		return run;
	}
	
	public void startTrackingRun(Run run) {
		// Получение идентификатора
		mCurrentId = run.getId();
		// Сохранение его в общих настройках
		mPrefs.edit().putLong(PREF_CURRENT_RUN_ID, mCurrentId).commit();
		// Запуск обновления данных местоположения
		startLocationUpdates();
	}
	
	public void stopRun() {
		stopLocationUpdates();
		mCurrentId = -1;
		mPrefs.edit().remove(PREF_CURRENT_RUN_ID).commit();
	}
	
	private Run insertRun() {
		Run run = new Run();
		run.setId(mHelper.insertRun(run));
		return run;
	}
	
	public RunCursor queryRuns(){
		return mHelper.QueryRuns();
	}
	
	public Run getRun(long id) {
		Run run = null;
		RunCursor cursor = mHelper.queryRun(id);
		cursor.moveToFirst();
		// Если строка присутствует, получить объект серии
		if (!cursor.isAfterLast())
			run = cursor.getRun();
		cursor.close();
		return run;
	}

	
	public void insertLocation(Step st){
		if(mCurrentId != -1){
			mHelper.insertLocation(mCurrentId, st);
		}else{
			Log.e(TAG, "Location received with no tracking run; ignoring.");
		}
	}

	public Location getLastLocationForRun(long runId) {
		Location location = null;
		LocationCursor cursor = mHelper.queryLastLocationForRun(runId);
		cursor.moveToFirst();
		// Если набор не пуст, получить местоположение
		if (!cursor.isAfterLast())
			location = cursor.getLocation();
		cursor.close();
		return location;
	}

	public LocationCursor queryLocationsForRun(long runId) {
		return mHelper.queryLocationsForRun(runId);
	}

	
}
